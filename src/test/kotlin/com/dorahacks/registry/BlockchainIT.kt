package com.dorahacks.registry
import com.dorahacks.registry.config.NodeConfiguration
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.*
import org.bouncycastle.jcajce.provider.digest.Keccak
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.web3j.protocol.core.DefaultBlockParameterName
import org.web3j.protocol.core.methods.response.AbiDefinition
import org.web3j.protocol.http.HttpService
import org.web3j.quorum.Quorum
import org.web3j.quorum.tx.ClientTransactionManager
import org.web3j.sample.contracts.generated.Asset
import org.web3j.sample.contracts.generated.Auditor
import org.web3j.sample.contracts.generated.Company
import org.web3j.sample.contracts.generated.Insurer
import org.web3j.tx.gas.ContractGasProvider
import org.web3j.tx.gas.DefaultGasProvider
import org.web3j.tx.gas.StaticGasProvider
import org.web3j.utils.Numeric
import java.math.BigInteger
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import java.util.concurrent.CountDownLatch
import java.util.stream.Collectors


@ContextConfiguration()
class BlockchainIT {

    private val gasProvider = StaticGasProvider(BigInteger.ZERO, DefaultGasProvider.GAS_LIMIT)

    private lateinit var quorum1: Quorum
    private lateinit var quorum2: Quorum
    private lateinit var quorum3: Quorum
    private lateinit var quorum4: Quorum

    @Before
    fun setup() {
        quorum1 = Quorum.build(HttpService("http://localhost:22001"))
        quorum2 = Quorum.build(HttpService("http://localhost:22002"))
        quorum3 = Quorum.build(HttpService("http://localhost:22003"))
        quorum4 = Quorum.build(HttpService("http://localhost:22004"))
    }

    @Test
    fun testConnection() {
        assertThat(quorum1.ethAccounts().send().accounts[0],
                `is`("0xed9d02e382b34818e88b88a309c7fe71e65f419d"))
        assertThat(quorum2.ethAccounts().send().accounts[0],
                `is`("0xca843569e3427144cead5e4d5999a3d0ccf92b8e"))
        assertThat(quorum3.ethAccounts().send().accounts[0],
                `is`("0x0fbdc686b912d7722dc86510934589e0aaf3b55a"))
        assertThat(quorum4.ethAccounts().send().accounts[0],
                `is`("0x9186eb3d20cbd1f5f992a950d808c4495153abd5"))
    }

    @Test
    fun `deploy contracts`() {
        val ctm2 = ClientTransactionManager(quorum2,
                quorum2.ethAccounts().send().accounts[0],
                null,
                null)
        val insurer = Insurer.deploy(
                quorum2, ctm2,
                gasProvider).send()

        val ctm3 = ClientTransactionManager(quorum3,
                quorum3.ethAccounts().send().accounts[0],
                null,
                null)
        val auditor = Auditor.deploy(quorum2, ctm2,
                gasProvider).send()
        val ctm1 = ClientTransactionManager(quorum1,
                quorum1.ethAccounts().send().accounts[0],
                null,
                null)

        val company = Company.deploy(
                quorum1, ctm1,
                gasProvider).send()
        println(insurer.contractAddress)
        println(auditor.contractAddress)
        println(company.contractAddress)

    }


    @Test
    fun `end to end registry asset`() {
        val ctm2 = ClientTransactionManager(quorum2,
                quorum2.ethAccounts().send().accounts[0],
                null,
                null)

//        val insurer = Insurer.load("0xf89dc78d248f271c408f574bac13a4f09b5a4f2a",
//                quorum2, ctm2,
//                gasProvider)

        val insurer = Insurer.deploy(
                quorum2, ctm2,
                gasProvider).send()

        val ctm3 = ClientTransactionManager(quorum3,
                quorum3.ethAccounts().send().accounts[0],
                null,
                null)

//        val auditor = Auditor.load("0x81845481fd51efd88fd44e8983a60cecf3886552",
//                quorum2, ctm2,
//                gasProvider)
        val auditor = Auditor.deploy(quorum2, ctm2,
                gasProvider).send()


        val ctm1 = ClientTransactionManager(quorum1,
                quorum1.ethAccounts().send().accounts[0],
                null,
                null)

//        val company = Company.load("0x9d13c6d3afe1721beef56b55d303b09e021e27ab",
//                quorum1, ctm1,
//                gasProvider)

        val company = Company.deploy(
                quorum1, ctm1,
                gasProvider).send()

        println("Deploying insurer contract ... ")
        Thread.sleep(500)
        println("Deployed insurer contract address: " + insurer.contractAddress)
        println("Deploying auditor contract ... ")
        Thread.sleep(500)

        println("Deployed auditor contract address: " + auditor.contractAddress)
        println("Deploying company contract ... ")
        Thread.sleep(500)

        println("Deployed company contract address: " + company.contractAddress)
        var assetToInsure: String = ""
        val cdlInsure = CountDownLatch(1)
        val cdlRegister = CountDownLatch(1)

        insurer.insuranceRequestProcessedEventObservable(DefaultBlockParameterName.LATEST,
                DefaultBlockParameterName.LATEST).subscribe {
            println("insurance for " + it._asset + it.accepted)
        }

        company.assetInsuredEventObservable(DefaultBlockParameterName.LATEST,
                DefaultBlockParameterName.LATEST).subscribe {
            val a = Asset.load(it._address, quorum1, ctm1, gasProvider)
            println("Getting insured assets: ")
            println("Insured asset addres: " + a.contractAddress)
            println("Deployed asset serial number: " + a.serialNo.send())
            println("Asset insured: " + a.isInsured.send())
            cdlInsure.countDown()
        }

        println("Creating asset ...")
        company.assetRegisteredEventObservable(DefaultBlockParameterName.LATEST,
                DefaultBlockParameterName.LATEST). subscribe {
            val a = Asset.load(it._address, quorum1, ctm1, gasProvider)
            assetToInsure = a.contractAddress
            println("Asset created at address: " + a.contractAddress)
            cdlRegister.countDown()
        }

        company.registerAsset(auditor.contractAddress, BigInteger.valueOf(3),
                BigInteger.TEN, BigInteger.ONE,
                "123").send()

        cdlRegister.await()

        val asset = Asset.load(assetToInsure, quorum1, ctm1, gasProvider)

        println("Asset is insured: " + asset.isInsured.send())

        company.insureAsset(assetToInsure, insurer.contractAddress).send()

        cdlInsure.await()


    }
}
