pragma solidity ^0.4.24;

import "./lib/open-zeppelin/ownership/Ownable.sol";
import "./lib/EnumLib.sol";
import "./interfaces/AuditorInterface.sol";
import "./Company.sol";
import "./Asset.sol";
import "./events/AuditorEvents.sol";

contract Auditor is Ownable, AuditorInterface, AuditorEvents {

    // address of company => address of asset => audited
    mapping(address => mapping(address => bool)) auditedAssets;

    function registerAsset (
        uint256 _serialNo,
        uint256 _value,
        EnumLib.Currency _symbol,
        // receipt hash start
        string ipfsKey
        // end
    )
    public {
        // TODO: require criteria for registering

        Company c = Company(msg.sender);
        // change to interface
        Asset a = new Asset(_serialNo,_value, _symbol, ipfsKey);
        a.transferOwnership(msg.sender);

        auditedAssets[msg.sender][a] = true;
        c.registerSuccess(a);
        emit AuditRequestProcessed(a, true);
    }
}
