pragma solidity ^0.4.24;

import "./interfaces/AssetInterface.sol";
import "./lib/open-zeppelin/ownership/Ownable.sol";
import "./lib/EnumLib.sol";

contract Asset is AssetInterface, Ownable {
// contract Asset {

    uint256 public serialNo;
    // absolute value of asset
    uint256 public value;

    // currency of value
    EnumLib.Currency public symbol;

    string public ipfsKey;


    bool public isInsured = false;


    function insure() public {
        isInsured = true;
    }

    function removeInsurance() public {
        isInsured = false;
    }

    constructor (uint256 _serialNo, uint256 _value, EnumLib.Currency _symbol, string _ipfsKey) public {
        serialNo = _serialNo;
        value = _value;
        symbol = _symbol;
        ipfsKey = _ipfsKey;
    }

    function kill() public onlyOwner {
        kill();
    }

    function getSerialNo() public view returns(uint256) {
        return serialNo;
    }
}
