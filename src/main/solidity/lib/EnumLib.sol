pragma solidity ^0.4.24;

contract EnumLib {
    // currency of asset value
    enum Currency {USD, GBP, EUR, BRL}
    // employee access level
    enum AccessLevel { Owner, Whitelisted, Employee }
    enum RegisterAssetFail {InvalidSerialNo}
    enum InsureAssetFail {GenerationFailure}
    // state of request
    enum State { Processing, Accepted, Declined }
}
