pragma solidity ^0.4.24;

import "../lib/EnumLib.sol";

// requests to either auditor or insurer
contract CompanyEvents {

    event EmployeeAdded (
        EnumLib.AccessLevel accessLevel,
        address _address
    );

    event EmployeeRemoved (

    );

    event AssetRegistered (
        address _address
    );

    event AssetInsured (
        address _address
    );

    event RegisterAssetFail (
        EnumLib.RegisterAssetFail error
    );

    event AssigneeAdded (
        address _assignee,
        address _asset
    );

    event AssigneeRemoved (
        address _assignee,
        address _asset
    );

    event AssetInsureFailed (
        EnumLib.InsureAssetFail _fail
    );

}
