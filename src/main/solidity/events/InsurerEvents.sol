pragma solidity ^0.4.24;

import "../lib/EnumLib.sol";

// requests to either auditor or insurer
contract InsurerEvents {

    event InsuranceRequestProcessed (
        address _asset,
        bool accepted
    );

}
