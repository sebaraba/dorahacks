pragma solidity ^0.4.24;

import "../lib/EnumLib.sol";

// requests to either auditor or insurer
contract AuditorEvents {

    event AuditRequestProcessed (
        address _asset,
        bool accepted
    );

}
