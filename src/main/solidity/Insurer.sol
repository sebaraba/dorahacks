pragma solidity ^0.4.24;

import "./interfaces/InsurerInterface.sol";
import "./events/InsurerEvents.sol";
import "./Company.sol";
import "./lib/EnumLib.sol";

contract Insurer is InsurerInterface, InsurerEvents {
    int256 id;

    EnumLib.State state;

    function processInsuranceRequest (
        address asset
    )
    public {
        // Access the asset and analyse it
        //Asset assetContract = Asset(asset);
        Company c = Company(msg.sender);
        c.insureSuccess(asset);
        emit InsuranceRequestProcessed(asset, true);
    }
}
