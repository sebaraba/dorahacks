pragma solidity ^0.4.24;

import "./lib/open-zeppelin/token/ERC721/ERC721.sol";
import "./lib/open-zeppelin/ownership/Ownable.sol";
import "./lib/EnumLib.sol";
import "./events/CompanyEvents.sol";
import "./interfaces/AssetInterface.sol";
import "./interfaces/AuditorInterface.sol";
import "./interfaces/InsurerInterface.sol";


contract Company is ERC721, Ownable, CompanyEvents {
    // registered assets
    mapping(address => AssetInterface) private assets;

    mapping(address => Employee) private employees;


     struct Employee {
         EnumLib.AccessLevel accessLevel;
         mapping(address => bool) assignedTo;
     }

    constructor () public {
    }

    function registerAsset (
        address _auditor,
        uint256 _serialNo,
        uint256 _value,
        EnumLib.Currency _symbol,
        // receipt multihash
        string ipfsKey
    )
        public
        onlyOwner
    {
        AuditorInterface ai = AuditorInterface(_auditor);
        ai.registerAsset(
            _serialNo,
            _value,
            _symbol,
            // receipt hash start
            ipfsKey
            // end
        );
    }

    function registerSuccess(address _asset) public {
        assets[_asset] = AssetInterface(_asset);
        emit AssetRegistered(_asset);
    }

    function insureAsset(address _asset, address _insurer) public {
        InsurerInterface ii = InsurerInterface(_insurer);
        ii.processInsuranceRequest(_asset);
    }

    function insureSuccess(address _address) public {
        assets[_address].insure();
        emit AssetInsured(_address);
    }

    function addEmployee(EnumLib.AccessLevel _accessLevel, address _address) public onlyOwner {
        employees[_address] = Employee(_accessLevel);
        emit EmployeeAdded(_accessLevel, _address);
    }

    function assignAsset(address _assignee, address _asset) public onlyOwner {
        employees[_assignee].assignedTo[_asset] = true;
        emit AssigneeAdded(_assignee, _asset);
    }

    function deassignAsset(address _assignee, address _asset) public onlyOwner {
        employees[_assignee].assignedTo[_asset] = false;
        emit AssigneeRemoved(_assignee, _asset);
    }
}
