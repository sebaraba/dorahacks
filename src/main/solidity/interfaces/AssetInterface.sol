pragma solidity ^0.4.24;

contract AssetInterface {

    function insure() public;
    function removeInsurance() public;
    function getSerialNo() public view returns(uint256);
}
