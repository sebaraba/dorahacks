pragma solidity ^0.4.24;

import "../lib/EnumLib.sol";

contract AuditorInterface {


    function registerAsset (
        uint256 _serialNo,
        uint256 _value,
        EnumLib.Currency _symbol,
        // receipt hash start
        string ipfsKey
        // end
    )
        public;
}
