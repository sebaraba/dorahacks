package org.web3j.sample.contracts.generated;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.abi.datatypes.generated.Uint8;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;
import rx.Observable;
import rx.functions.Func1;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 3.6.0.
 */
public class Company extends Contract {
    private static final String BINARY = "608060405234801561001057600080fd5b506100437f01ffc9a7000000000000000000000000000000000000000000000000000000006401000000006100c6810204565b6100757f80ac58cd000000000000000000000000000000000000000000000000000000006401000000006100c6810204565b60058054600160a060020a031916331790819055604051600160a060020a0391909116906000907f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0908290a3610132565b7fffffffff0000000000000000000000000000000000000000000000000000000080821614156100f557600080fd5b7fffffffff00000000000000000000000000000000000000000000000000000000166000908152602081905260409020805460ff19166001179055565b611135806101416000396000f30060806040526004361061011c5763ffffffff7c010000000000000000000000000000000000000000000000000000000060003504166301ffc9a7811461012157806304a8a9bf1461016c578063081812fc14610195578063095ea7b3146101c957806323b872dd146101ed5780632a1c7cf9146102175780632a5776f71461028e5780632e0787eb146102b557806342842e0e146102dc5780634b21076a146103065780635018b290146103275780636352211e1461034e5780636d4763a01461036657806370a0823114610387578063715018a6146103ba5780638da5cb5b146103cf5780638f32d59b146103e4578063a22cb465146103f9578063b88d4fde1461041f578063e985e9c51461048e578063f2fde38b146104b5575b600080fd5b34801561012d57600080fd5b506101587bffffffffffffffffffffffffffffffffffffffffffffffffffffffff19600435166104d6565b604080519115158252519081900360200190f35b34801561017857600080fd5b50610193600160a060020a036004358116906024351661050a565b005b3480156101a157600080fd5b506101ad60043561058b565b60408051600160a060020a039092168252519081900360200190f35b3480156101d557600080fd5b50610193600160a060020a03600435166024356105bd565b3480156101f957600080fd5b50610193600160a060020a0360043581169060243516604435610666565b34801561022357600080fd5b50604080516020601f60843560048181013592830184900484028501840190955281845261019394600160a060020a038135169460248035956044359560ff606435169536959460a49490939101919081908401838280828437509497506106f49650505050505050565b34801561029a57600080fd5b50610193600160a060020a0360043581169060243516610816565b3480156102c157600080fd5b50610193600160a060020a036004358116906024351661089a565b3480156102e857600080fd5b50610193600160a060020a036004358116906024351660443561091f565b34801561031257600080fd5b50610193600160a060020a0360043516610940565b34801561033357600080fd5b5061019360ff60043516600160a060020a0360243516610a01565b34801561035a57600080fd5b506101ad600435610abf565b34801561037257600080fd5b50610193600160a060020a0360043516610ae9565b34801561039357600080fd5b506103a8600160a060020a0360043516610b49565b60408051918252519081900360200190f35b3480156103c657600080fd5b50610193610b7c565b3480156103db57600080fd5b506101ad610bd9565b3480156103f057600080fd5b50610158610be8565b34801561040557600080fd5b50610193600160a060020a03600435166024351515610bf9565b34801561042b57600080fd5b50604080516020601f60643560048181013592830184900484028501840190955281845261019394600160a060020a038135811695602480359092169560443595369560849401918190840183828082843750949750610c7d9650505050505050565b34801561049a57600080fd5b50610158600160a060020a0360043581169060243516610ca5565b3480156104c157600080fd5b50610193600160a060020a0360043516610cd3565b7bffffffffffffffffffffffffffffffffffffffffffffffffffffffff191660009081526020819052604090205460ff1690565b604080517f25edf735000000000000000000000000000000000000000000000000000000008152600160a060020a038481166004830152915183928316916325edf73591602480830192600092919082900301818387803b15801561056e57600080fd5b505af1158015610582573d6000803e3d6000fd5b50505050505050565b600061059682610cf2565b15156105a157600080fd5b50600090815260026020526040902054600160a060020a031690565b60006105c882610abf565b9050600160a060020a0383811690821614156105e357600080fd5b33600160a060020a03821614806105ff57506105ff8133610ca5565b151561060a57600080fd5b6000828152600260205260408082208054600160a060020a031916600160a060020a0387811691821790925591518593918516917f8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b92591a4505050565b6106703382610d0f565b151561067b57600080fd5b600160a060020a038216151561069057600080fd5b61069a8382610d6e565b6106a48382610dd2565b6106ae8282610e5b565b8082600160a060020a031684600160a060020a03167fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef60405160405180910390a4505050565b60006106fe610be8565b151561070957600080fd5b85905080600160a060020a031663dba47038868686866040518563ffffffff167c01000000000000000000000000000000000000000000000000000000000281526004018085815260200184815260200183600381111561076657fe5b60ff16815260200180602001828103825283818151815260200191508051906020019080838360005b838110156107a757818101518382015260200161078f565b50505050905090810190601f1680156107d45780820380516001836020036101000a031916815260200191505b5095505050505050600060405180830381600087803b1580156107f657600080fd5b505af115801561080a573d6000803e3d6000fd5b50505050505050505050565b61081e610be8565b151561082957600080fd5b600160a060020a0380831660008181526007602090815260408083209486168084526001909501825291829020805460ff19169055815192835282019290925281517f47d29a872db18a0331d15d141b3130dd84d9f8b6641575c66b7ef8225cabe496929181900390910190a15050565b6108a2610be8565b15156108ad57600080fd5b600160a060020a0380831660008181526007602090815260408083209486168084526001958601835292819020805460ff1916909517909455835192835282015281517f350f7e55144b8683e07987ea9cee47183d03cfb8652c290237cce1ac9dd17f0e929181900390910190a15050565b61093b8383836020604051908101604052806000815250610c7d565b505050565b600160a060020a038082166000908152600660205260408082205481517f3c3ce17d0000000000000000000000000000000000000000000000000000000081529151931692633c3ce17d9260048084019391929182900301818387803b1580156109a957600080fd5b505af11580156109bd573d6000803e3d6000fd5b505060408051600160a060020a038516815290517fb559661f3f697a3dd89a869b2d43b56f7fb593a56609df5a179bd63b39c9f0cf9350908190036020019150a150565b610a09610be8565b1515610a1457600080fd5b602060405190810160405280836002811115610a2c57fe5b9052600160a060020a038216600090815260076020526040902081518154829060ff19166001836002811115610a5e57fe5b02179055509050507f99ec4d1c1ab0561392c43378dac61a826724bf474d5311ffd3c734653a0fd28d828260405180836002811115610a9957fe5b60ff168152600160a060020a039092166020830152506040805191829003019150a15050565b600081815260016020526040812054600160a060020a0316801515610ae357600080fd5b92915050565b600160a060020a0381166000818152600660209081526040918290208054600160a060020a03191684179055815192835290517f0700b0f4c19e631ed321192cfc0b71a83caa514675497afc74fe817d5f839dd59281900390910190a150565b6000600160a060020a0382161515610b6057600080fd5b50600160a060020a031660009081526003602052604090205490565b610b84610be8565b1515610b8f57600080fd5b600554604051600091600160a060020a0316907f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0908390a360058054600160a060020a0319169055565b600554600160a060020a031690565b600554600160a060020a0316331490565b600160a060020a038216331415610c0f57600080fd5b336000818152600460209081526040808320600160a060020a03871680855290835292819020805460ff1916861515908117909155815190815290519293927f17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31929181900390910190a35050565b610c88848484610666565b610c9484848484610ede565b1515610c9f57600080fd5b50505050565b600160a060020a03918216600090815260046020908152604080832093909416825291909152205460ff1690565b610cdb610be8565b1515610ce657600080fd5b610cef81611060565b50565b600090815260016020526040902054600160a060020a0316151590565b600080610d1b83610abf565b905080600160a060020a031684600160a060020a03161480610d56575083600160a060020a0316610d4b8461058b565b600160a060020a0316145b80610d665750610d668185610ca5565b949350505050565b81600160a060020a0316610d8182610abf565b600160a060020a031614610d9457600080fd5b600081815260026020526040902054600160a060020a031615610dce5760008181526002602052604090208054600160a060020a03191690555b5050565b81600160a060020a0316610de582610abf565b600160a060020a031614610df857600080fd5b600160a060020a038216600090815260036020526040902054610e2290600163ffffffff6110d116565b600160a060020a039092166000908152600360209081526040808320949094559181526001909152208054600160a060020a0319169055565b600081815260016020526040902054600160a060020a031615610e7d57600080fd5b60008181526001602081815260408084208054600160a060020a031916600160a060020a0388169081179091558452600390915290912054610ebe916110e8565b600160a060020a0390921660009081526003602052604090209190915550565b600080610ef385600160a060020a0316611101565b1515610f025760019150611057565b6040517f150b7a020000000000000000000000000000000000000000000000000000000081523360048201818152600160a060020a03898116602485015260448401889052608060648501908152875160848601528751918a169463150b7a0294938c938b938b93909160a490910190602085019080838360005b83811015610f95578181015183820152602001610f7d565b50505050905090810190601f168015610fc25780820380516001836020036101000a031916815260200191505b5095505050505050602060405180830381600087803b158015610fe457600080fd5b505af1158015610ff8573d6000803e3d6000fd5b505050506040513d602081101561100e57600080fd5b50517bffffffffffffffffffffffffffffffffffffffffffffffffffffffff1981167f150b7a020000000000000000000000000000000000000000000000000000000014925090505b50949350505050565b600160a060020a038116151561107557600080fd5b600554604051600160a060020a038084169216907f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e090600090a360058054600160a060020a031916600160a060020a0392909216919091179055565b600080838311156110e157600080fd5b5050900390565b6000828201838110156110fa57600080fd5b9392505050565b6000903b11905600a165627a7a7230582082fd247db7eebd304e8cdd775c674837c9a473d7d00a79f2885c33450ac3e0b30029";

    public static final String FUNC_SUPPORTSINTERFACE = "supportsInterface";

    public static final String FUNC_INSUREASSET = "insureAsset";

    public static final String FUNC_GETAPPROVED = "getApproved";

    public static final String FUNC_APPROVE = "approve";

    public static final String FUNC_TRANSFERFROM = "transferFrom";

    public static final String FUNC_REGISTERASSET = "registerAsset";

    public static final String FUNC_DEASSIGNASSET = "deassignAsset";

    public static final String FUNC_ASSIGNASSET = "assignAsset";

    public static final String FUNC_SAFETRANSFERFROM = "safeTransferFrom";

    public static final String FUNC_INSURESUCCESS = "insureSuccess";

    public static final String FUNC_ADDEMPLOYEE = "addEmployee";

    public static final String FUNC_OWNEROF = "ownerOf";

    public static final String FUNC_REGISTERSUCCESS = "registerSuccess";

    public static final String FUNC_BALANCEOF = "balanceOf";

    public static final String FUNC_RENOUNCEOWNERSHIP = "renounceOwnership";

    public static final String FUNC_OWNER = "owner";

    public static final String FUNC_ISOWNER = "isOwner";

    public static final String FUNC_SETAPPROVALFORALL = "setApprovalForAll";

    public static final String FUNC_ISAPPROVEDFORALL = "isApprovedForAll";

    public static final String FUNC_TRANSFEROWNERSHIP = "transferOwnership";

    public static final Event EMPLOYEEADDED_EVENT = new Event("EmployeeAdded", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint8>() {}, new TypeReference<Address>() {}));
    ;

    public static final Event EMPLOYEEREMOVED_EVENT = new Event("EmployeeRemoved", 
            Arrays.<TypeReference<?>>asList());
    ;

    public static final Event ASSETREGISTERED_EVENT = new Event("AssetRegistered", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
    ;

    public static final Event ASSETINSURED_EVENT = new Event("AssetInsured", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
    ;

    public static final Event REGISTERASSETFAIL_EVENT = new Event("RegisterAssetFail", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint8>() {}));
    ;

    public static final Event ASSIGNEEADDED_EVENT = new Event("AssigneeAdded", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Address>() {}));
    ;

    public static final Event ASSIGNEEREMOVED_EVENT = new Event("AssigneeRemoved", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Address>() {}));
    ;

    public static final Event ASSETINSUREFAILED_EVENT = new Event("AssetInsureFailed", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint8>() {}));
    ;

    public static final Event OWNERSHIPTRANSFERRED_EVENT = new Event("OwnershipTransferred", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}));
    ;

    public static final Event TRANSFER_EVENT = new Event("Transfer", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}, new TypeReference<Uint256>(true) {}));
    ;

    public static final Event APPROVAL_EVENT = new Event("Approval", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}, new TypeReference<Uint256>(true) {}));
    ;

    public static final Event APPROVALFORALL_EVENT = new Event("ApprovalForAll", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}, new TypeReference<Bool>() {}));
    ;

    @Deprecated
    protected Company(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected Company(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected Company(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected Company(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public RemoteCall<Boolean> supportsInterface(byte[] interfaceId) {
        final Function function = new Function(FUNC_SUPPORTSINTERFACE, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Bytes4(interfaceId)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteCall<TransactionReceipt> insureAsset(String _asset, String _insurer) {
        final Function function = new Function(
                FUNC_INSUREASSET, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(_asset), 
                new org.web3j.abi.datatypes.Address(_insurer)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<String> getApproved(BigInteger tokenId) {
        final Function function = new Function(FUNC_GETAPPROVED, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(tokenId)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<TransactionReceipt> approve(String to, BigInteger tokenId) {
        final Function function = new Function(
                FUNC_APPROVE, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(to), 
                new org.web3j.abi.datatypes.generated.Uint256(tokenId)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> transferFrom(String from, String to, BigInteger tokenId) {
        final Function function = new Function(
                FUNC_TRANSFERFROM, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(from), 
                new org.web3j.abi.datatypes.Address(to), 
                new org.web3j.abi.datatypes.generated.Uint256(tokenId)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> registerAsset(String _auditor, BigInteger _serialNo, BigInteger _value, BigInteger _symbol, String ipfsKey) {
        final Function function = new Function(
                FUNC_REGISTERASSET, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(_auditor), 
                new org.web3j.abi.datatypes.generated.Uint256(_serialNo), 
                new org.web3j.abi.datatypes.generated.Uint256(_value), 
                new org.web3j.abi.datatypes.generated.Uint8(_symbol), 
                new org.web3j.abi.datatypes.Utf8String(ipfsKey)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> deassignAsset(String _assignee, String _asset) {
        final Function function = new Function(
                FUNC_DEASSIGNASSET, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(_assignee), 
                new org.web3j.abi.datatypes.Address(_asset)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> assignAsset(String _assignee, String _asset) {
        final Function function = new Function(
                FUNC_ASSIGNASSET, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(_assignee), 
                new org.web3j.abi.datatypes.Address(_asset)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> safeTransferFrom(String from, String to, BigInteger tokenId) {
        final Function function = new Function(
                FUNC_SAFETRANSFERFROM, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(from), 
                new org.web3j.abi.datatypes.Address(to), 
                new org.web3j.abi.datatypes.generated.Uint256(tokenId)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> insureSuccess(String _address) {
        final Function function = new Function(
                FUNC_INSURESUCCESS, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(_address)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> addEmployee(BigInteger _accessLevel, String _address) {
        final Function function = new Function(
                FUNC_ADDEMPLOYEE, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint8(_accessLevel), 
                new org.web3j.abi.datatypes.Address(_address)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<String> ownerOf(BigInteger tokenId) {
        final Function function = new Function(FUNC_OWNEROF, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(tokenId)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<TransactionReceipt> registerSuccess(String _asset) {
        final Function function = new Function(
                FUNC_REGISTERSUCCESS, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(_asset)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<BigInteger> balanceOf(String owner) {
        final Function function = new Function(FUNC_BALANCEOF, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(owner)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<TransactionReceipt> renounceOwnership() {
        final Function function = new Function(
                FUNC_RENOUNCEOWNERSHIP, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<String> owner() {
        final Function function = new Function(FUNC_OWNER, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<Boolean> isOwner() {
        final Function function = new Function(FUNC_ISOWNER, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteCall<TransactionReceipt> setApprovalForAll(String to, Boolean approved) {
        final Function function = new Function(
                FUNC_SETAPPROVALFORALL, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(to), 
                new org.web3j.abi.datatypes.Bool(approved)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> safeTransferFrom(String from, String to, BigInteger tokenId, byte[] _data) {
        final Function function = new Function(
                FUNC_SAFETRANSFERFROM, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(from), 
                new org.web3j.abi.datatypes.Address(to), 
                new org.web3j.abi.datatypes.generated.Uint256(tokenId), 
                new org.web3j.abi.datatypes.DynamicBytes(_data)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<Boolean> isApprovedForAll(String owner, String operator) {
        final Function function = new Function(FUNC_ISAPPROVEDFORALL, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(owner), 
                new org.web3j.abi.datatypes.Address(operator)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteCall<TransactionReceipt> transferOwnership(String newOwner) {
        final Function function = new Function(
                FUNC_TRANSFEROWNERSHIP, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(newOwner)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public static RemoteCall<Company> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(Company.class, web3j, credentials, contractGasProvider, BINARY, "");
    }

    public static RemoteCall<Company> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(Company.class, web3j, transactionManager, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<Company> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Company.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<Company> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Company.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    public List<EmployeeAddedEventResponse> getEmployeeAddedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(EMPLOYEEADDED_EVENT, transactionReceipt);
        ArrayList<EmployeeAddedEventResponse> responses = new ArrayList<EmployeeAddedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            EmployeeAddedEventResponse typedResponse = new EmployeeAddedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.accessLevel = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse._address = (String) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<EmployeeAddedEventResponse> employeeAddedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, EmployeeAddedEventResponse>() {
            @Override
            public EmployeeAddedEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(EMPLOYEEADDED_EVENT, log);
                EmployeeAddedEventResponse typedResponse = new EmployeeAddedEventResponse();
                typedResponse.log = log;
                typedResponse.accessLevel = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse._address = (String) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<EmployeeAddedEventResponse> employeeAddedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(EMPLOYEEADDED_EVENT));
        return employeeAddedEventObservable(filter);
    }

    public List<EmployeeRemovedEventResponse> getEmployeeRemovedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(EMPLOYEEREMOVED_EVENT, transactionReceipt);
        ArrayList<EmployeeRemovedEventResponse> responses = new ArrayList<EmployeeRemovedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            EmployeeRemovedEventResponse typedResponse = new EmployeeRemovedEventResponse();
            typedResponse.log = eventValues.getLog();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<EmployeeRemovedEventResponse> employeeRemovedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, EmployeeRemovedEventResponse>() {
            @Override
            public EmployeeRemovedEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(EMPLOYEEREMOVED_EVENT, log);
                EmployeeRemovedEventResponse typedResponse = new EmployeeRemovedEventResponse();
                typedResponse.log = log;
                return typedResponse;
            }
        });
    }

    public Observable<EmployeeRemovedEventResponse> employeeRemovedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(EMPLOYEEREMOVED_EVENT));
        return employeeRemovedEventObservable(filter);
    }

    public List<AssetRegisteredEventResponse> getAssetRegisteredEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(ASSETREGISTERED_EVENT, transactionReceipt);
        ArrayList<AssetRegisteredEventResponse> responses = new ArrayList<AssetRegisteredEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            AssetRegisteredEventResponse typedResponse = new AssetRegisteredEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse._address = (String) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<AssetRegisteredEventResponse> assetRegisteredEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, AssetRegisteredEventResponse>() {
            @Override
            public AssetRegisteredEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(ASSETREGISTERED_EVENT, log);
                AssetRegisteredEventResponse typedResponse = new AssetRegisteredEventResponse();
                typedResponse.log = log;
                typedResponse._address = (String) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<AssetRegisteredEventResponse> assetRegisteredEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(ASSETREGISTERED_EVENT));
        return assetRegisteredEventObservable(filter);
    }

    public List<AssetInsuredEventResponse> getAssetInsuredEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(ASSETINSURED_EVENT, transactionReceipt);
        ArrayList<AssetInsuredEventResponse> responses = new ArrayList<AssetInsuredEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            AssetInsuredEventResponse typedResponse = new AssetInsuredEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse._address = (String) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<AssetInsuredEventResponse> assetInsuredEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, AssetInsuredEventResponse>() {
            @Override
            public AssetInsuredEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(ASSETINSURED_EVENT, log);
                AssetInsuredEventResponse typedResponse = new AssetInsuredEventResponse();
                typedResponse.log = log;
                typedResponse._address = (String) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<AssetInsuredEventResponse> assetInsuredEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(ASSETINSURED_EVENT));
        return assetInsuredEventObservable(filter);
    }

    public List<RegisterAssetFailEventResponse> getRegisterAssetFailEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(REGISTERASSETFAIL_EVENT, transactionReceipt);
        ArrayList<RegisterAssetFailEventResponse> responses = new ArrayList<RegisterAssetFailEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            RegisterAssetFailEventResponse typedResponse = new RegisterAssetFailEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.error = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<RegisterAssetFailEventResponse> registerAssetFailEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, RegisterAssetFailEventResponse>() {
            @Override
            public RegisterAssetFailEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(REGISTERASSETFAIL_EVENT, log);
                RegisterAssetFailEventResponse typedResponse = new RegisterAssetFailEventResponse();
                typedResponse.log = log;
                typedResponse.error = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<RegisterAssetFailEventResponse> registerAssetFailEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(REGISTERASSETFAIL_EVENT));
        return registerAssetFailEventObservable(filter);
    }

    public List<AssigneeAddedEventResponse> getAssigneeAddedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(ASSIGNEEADDED_EVENT, transactionReceipt);
        ArrayList<AssigneeAddedEventResponse> responses = new ArrayList<AssigneeAddedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            AssigneeAddedEventResponse typedResponse = new AssigneeAddedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse._assignee = (String) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse._asset = (String) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<AssigneeAddedEventResponse> assigneeAddedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, AssigneeAddedEventResponse>() {
            @Override
            public AssigneeAddedEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(ASSIGNEEADDED_EVENT, log);
                AssigneeAddedEventResponse typedResponse = new AssigneeAddedEventResponse();
                typedResponse.log = log;
                typedResponse._assignee = (String) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse._asset = (String) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<AssigneeAddedEventResponse> assigneeAddedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(ASSIGNEEADDED_EVENT));
        return assigneeAddedEventObservable(filter);
    }

    public List<AssigneeRemovedEventResponse> getAssigneeRemovedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(ASSIGNEEREMOVED_EVENT, transactionReceipt);
        ArrayList<AssigneeRemovedEventResponse> responses = new ArrayList<AssigneeRemovedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            AssigneeRemovedEventResponse typedResponse = new AssigneeRemovedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse._assignee = (String) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse._asset = (String) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<AssigneeRemovedEventResponse> assigneeRemovedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, AssigneeRemovedEventResponse>() {
            @Override
            public AssigneeRemovedEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(ASSIGNEEREMOVED_EVENT, log);
                AssigneeRemovedEventResponse typedResponse = new AssigneeRemovedEventResponse();
                typedResponse.log = log;
                typedResponse._assignee = (String) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse._asset = (String) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<AssigneeRemovedEventResponse> assigneeRemovedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(ASSIGNEEREMOVED_EVENT));
        return assigneeRemovedEventObservable(filter);
    }

    public List<AssetInsureFailedEventResponse> getAssetInsureFailedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(ASSETINSUREFAILED_EVENT, transactionReceipt);
        ArrayList<AssetInsureFailedEventResponse> responses = new ArrayList<AssetInsureFailedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            AssetInsureFailedEventResponse typedResponse = new AssetInsureFailedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse._fail = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<AssetInsureFailedEventResponse> assetInsureFailedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, AssetInsureFailedEventResponse>() {
            @Override
            public AssetInsureFailedEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(ASSETINSUREFAILED_EVENT, log);
                AssetInsureFailedEventResponse typedResponse = new AssetInsureFailedEventResponse();
                typedResponse.log = log;
                typedResponse._fail = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<AssetInsureFailedEventResponse> assetInsureFailedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(ASSETINSUREFAILED_EVENT));
        return assetInsureFailedEventObservable(filter);
    }

    public List<OwnershipTransferredEventResponse> getOwnershipTransferredEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, transactionReceipt);
        ArrayList<OwnershipTransferredEventResponse> responses = new ArrayList<OwnershipTransferredEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<OwnershipTransferredEventResponse> ownershipTransferredEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, OwnershipTransferredEventResponse>() {
            @Override
            public OwnershipTransferredEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, log);
                OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
                typedResponse.log = log;
                typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<OwnershipTransferredEventResponse> ownershipTransferredEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(OWNERSHIPTRANSFERRED_EVENT));
        return ownershipTransferredEventObservable(filter);
    }

    public List<TransferEventResponse> getTransferEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(TRANSFER_EVENT, transactionReceipt);
        ArrayList<TransferEventResponse> responses = new ArrayList<TransferEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            TransferEventResponse typedResponse = new TransferEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.from = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.to = (String) eventValues.getIndexedValues().get(1).getValue();
            typedResponse.tokenId = (BigInteger) eventValues.getIndexedValues().get(2).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<TransferEventResponse> transferEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, TransferEventResponse>() {
            @Override
            public TransferEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(TRANSFER_EVENT, log);
                TransferEventResponse typedResponse = new TransferEventResponse();
                typedResponse.log = log;
                typedResponse.from = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.to = (String) eventValues.getIndexedValues().get(1).getValue();
                typedResponse.tokenId = (BigInteger) eventValues.getIndexedValues().get(2).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<TransferEventResponse> transferEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(TRANSFER_EVENT));
        return transferEventObservable(filter);
    }

    public List<ApprovalEventResponse> getApprovalEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(APPROVAL_EVENT, transactionReceipt);
        ArrayList<ApprovalEventResponse> responses = new ArrayList<ApprovalEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            ApprovalEventResponse typedResponse = new ApprovalEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.owner = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.approved = (String) eventValues.getIndexedValues().get(1).getValue();
            typedResponse.tokenId = (BigInteger) eventValues.getIndexedValues().get(2).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<ApprovalEventResponse> approvalEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, ApprovalEventResponse>() {
            @Override
            public ApprovalEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(APPROVAL_EVENT, log);
                ApprovalEventResponse typedResponse = new ApprovalEventResponse();
                typedResponse.log = log;
                typedResponse.owner = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.approved = (String) eventValues.getIndexedValues().get(1).getValue();
                typedResponse.tokenId = (BigInteger) eventValues.getIndexedValues().get(2).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<ApprovalEventResponse> approvalEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(APPROVAL_EVENT));
        return approvalEventObservable(filter);
    }

    public List<ApprovalForAllEventResponse> getApprovalForAllEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(APPROVALFORALL_EVENT, transactionReceipt);
        ArrayList<ApprovalForAllEventResponse> responses = new ArrayList<ApprovalForAllEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            ApprovalForAllEventResponse typedResponse = new ApprovalForAllEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.owner = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.operator = (String) eventValues.getIndexedValues().get(1).getValue();
            typedResponse.approved = (Boolean) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<ApprovalForAllEventResponse> approvalForAllEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, ApprovalForAllEventResponse>() {
            @Override
            public ApprovalForAllEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(APPROVALFORALL_EVENT, log);
                ApprovalForAllEventResponse typedResponse = new ApprovalForAllEventResponse();
                typedResponse.log = log;
                typedResponse.owner = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.operator = (String) eventValues.getIndexedValues().get(1).getValue();
                typedResponse.approved = (Boolean) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<ApprovalForAllEventResponse> approvalForAllEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(APPROVALFORALL_EVENT));
        return approvalForAllEventObservable(filter);
    }

    @Deprecated
    public static Company load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new Company(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static Company load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new Company(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static Company load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new Company(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static Company load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new Company(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static class EmployeeAddedEventResponse {
        public Log log;

        public BigInteger accessLevel;

        public String _address;
    }

    public static class EmployeeRemovedEventResponse {
        public Log log;
    }

    public static class AssetRegisteredEventResponse {
        public Log log;

        public String _address;
    }

    public static class AssetInsuredEventResponse {
        public Log log;

        public String _address;
    }

    public static class RegisterAssetFailEventResponse {
        public Log log;

        public BigInteger error;
    }

    public static class AssigneeAddedEventResponse {
        public Log log;

        public String _assignee;

        public String _asset;
    }

    public static class AssigneeRemovedEventResponse {
        public Log log;

        public String _assignee;

        public String _asset;
    }

    public static class AssetInsureFailedEventResponse {
        public Log log;

        public BigInteger _fail;
    }

    public static class OwnershipTransferredEventResponse {
        public Log log;

        public String previousOwner;

        public String newOwner;
    }

    public static class TransferEventResponse {
        public Log log;

        public String from;

        public String to;

        public BigInteger tokenId;
    }

    public static class ApprovalEventResponse {
        public Log log;

        public String owner;

        public String approved;

        public BigInteger tokenId;
    }

    public static class ApprovalForAllEventResponse {
        public Log log;

        public String owner;

        public String operator;

        public Boolean approved;
    }
}
