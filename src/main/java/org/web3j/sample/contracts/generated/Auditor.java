package org.web3j.sample.contracts.generated;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;
import rx.Observable;
import rx.functions.Func1;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 3.6.0.
 */
public class Auditor extends Contract {
    private static final String BINARY = "6080604081905260008054600160a060020a0319163317808255600160a060020a0316917f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0908290a3610b79806100576000396000f30060806040526004361061006c5763ffffffff7c0100000000000000000000000000000000000000000000000000000000600035041663715018a681146100715780638da5cb5b146100885780638f32d59b146100b9578063dba47038146100e2578063f2fde38b14610148575b600080fd5b34801561007d57600080fd5b50610086610169565b005b34801561009457600080fd5b5061009d6101d3565b60408051600160a060020a039092168252519081900360200190f35b3480156100c557600080fd5b506100ce6101e2565b604080519115158252519081900360200190f35b3480156100ee57600080fd5b50604080516020601f60643560048181013592830184900484028501840190955281845261008694803594602480359560ff6044351695369560849493019181908401838280828437509497506101f39650505050505050565b34801561015457600080fd5b50610086600160a060020a0360043516610427565b6101716101e2565b151561017c57600080fd5b60008054604051600160a060020a03909116907f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0908390a36000805473ffffffffffffffffffffffffffffffffffffffff19169055565b600054600160a060020a031690565b600054600160a060020a0316331490565b336000858585856102026104c3565b8085815260200184815260200183600381111561021b57fe5b60ff16815260200180602001828103825283818151815260200191508051906020019080838360005b8381101561025c578181015183820152602001610244565b50505050905090810190601f1680156102895780820380516001836020036101000a031916815260200191505b5095505050505050604051809103906000f0801580156102ad573d6000803e3d6000fd5b50905080600160a060020a031663f2fde38b836040518263ffffffff167c01000000000000000000000000000000000000000000000000000000000281526004018082600160a060020a0316600160a060020a03168152602001915050600060405180830381600087803b15801561032457600080fd5b505af1158015610338573d6000803e3d6000fd5b5050336000908152600160208181526040808420600160a060020a0388811680875291909352818520805460ff191690941790935580517f6d4763a00000000000000000000000000000000000000000000000000000000081526004810193909352519087169450636d4763a093506024808301939282900301818387803b1580156103c357600080fd5b505af11580156103d7573d6000803e3d6000fd5b505060408051600160a060020a03851681526001602082015281517fb79319bc0ae3bfd68fa25ce05c7493e794456f48ef375a3a9db49cb035c6bac89450908190039091019150a1505050505050565b61042f6101e2565b151561043a57600080fd5b61044381610446565b50565b600160a060020a038116151561045b57600080fd5b60008054604051600160a060020a03808516939216917f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e091a36000805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0392909216919091179055565b60405161067a806104d483390190560060806040526005805460ff1916905534801561001a57600080fd5b5060405161067a38038061067a8339810160408181528251602084015191840151606085015160008054600160a060020a03191633178082559396949592949190910192600160a060020a0316917f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0908290a36001848155600284905560038054849260ff199091169083838111156100af57fe5b021790555080516100c79060049060208401906100d1565b505050505061016c565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061011257805160ff191683800117855561013f565b8280016001018555821561013f579182015b8281111561013f578251825591602001919060010190610124565b5061014b92915061014f565b5090565b61016991905b8082111561014b5760008155600101610155565b90565b6104ff8061017b6000396000f3006080604052600436106100c45763ffffffff7c01000000000000000000000000000000000000000000000000000000006000350416630a0713c181146100c95780633c3ce17d146101535780633fa4f2451461016a57806341c0e1b51461019157806344e52e29146101a6578063660f17fe146101bb578063715018a6146101d05780638da5cb5b146101e55780638f32d59b1461021657806395d89b411461023f578063ac6f73a514610278578063f20981ad1461028d578063f2fde38b146102a2575b600080fd5b3480156100d557600080fd5b506100de6102c3565b6040805160208082528351818301528351919283929083019185019080838360005b83811015610118578181015183820152602001610100565b50505050905090810190601f1680156101455780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b34801561015f57600080fd5b50610168610351565b005b34801561017657600080fd5b5061017f610360565b60408051918252519081900360200190f35b34801561019d57600080fd5b50610168610366565b3480156101b257600080fd5b5061017f610383565b3480156101c757600080fd5b5061017f610389565b3480156101dc57600080fd5b5061016861038f565b3480156101f157600080fd5b506101fa6103f9565b60408051600160a060020a039092168252519081900360200190f35b34801561022257600080fd5b5061022b610408565b604080519115158252519081900360200190f35b34801561024b57600080fd5b50610254610419565b6040518082600381111561026457fe5b60ff16815260200191505060405180910390f35b34801561028457600080fd5b50610168610422565b34801561029957600080fd5b5061022b61042e565b3480156102ae57600080fd5b50610168600160a060020a0360043516610437565b6004805460408051602060026001851615610100026000190190941693909304601f810184900484028201840190925281815292918301828280156103495780601f1061031e57610100808354040283529160200191610349565b820191906000526020600020905b81548152906001019060200180831161032c57829003601f168201915b505050505081565b6005805460ff19166001179055565b60025481565b61036e610408565b151561037957600080fd5b610381610366565b565b60015490565b60015481565b610397610408565b15156103a257600080fd5b60008054604051600160a060020a03909116907f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0908390a36000805473ffffffffffffffffffffffffffffffffffffffff19169055565b600054600160a060020a031690565b600054600160a060020a0316331490565b60035460ff1681565b6005805460ff19169055565b60055460ff1681565b61043f610408565b151561044a57600080fd5b61045381610456565b50565b600160a060020a038116151561046b57600080fd5b60008054604051600160a060020a03808516939216917f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e091a36000805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a03929092169190911790555600a165627a7a7230582059cd1e1472fb2629e86d8290e25ec2b94b72d5c975d6ab12b7eff15b2ac8d7250029a165627a7a723058200fa47fcae309c937051e735d26a759f2dcf5c7531aa4034f5d70f339583fc5c30029";

    public static final String FUNC_RENOUNCEOWNERSHIP = "renounceOwnership";

    public static final String FUNC_OWNER = "owner";

    public static final String FUNC_ISOWNER = "isOwner";

    public static final String FUNC_REGISTERASSET = "registerAsset";

    public static final String FUNC_TRANSFEROWNERSHIP = "transferOwnership";

    public static final Event AUDITREQUESTPROCESSED_EVENT = new Event("AuditRequestProcessed", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Bool>() {}));
    ;

    public static final Event OWNERSHIPTRANSFERRED_EVENT = new Event("OwnershipTransferred", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}));
    ;

    @Deprecated
    protected Auditor(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected Auditor(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected Auditor(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected Auditor(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public RemoteCall<TransactionReceipt> renounceOwnership() {
        final Function function = new Function(
                FUNC_RENOUNCEOWNERSHIP, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<String> owner() {
        final Function function = new Function(FUNC_OWNER, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<Boolean> isOwner() {
        final Function function = new Function(FUNC_ISOWNER, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteCall<TransactionReceipt> registerAsset(BigInteger _serialNo, BigInteger _value, BigInteger _symbol, String ipfsKey) {
        final Function function = new Function(
                FUNC_REGISTERASSET, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_serialNo), 
                new org.web3j.abi.datatypes.generated.Uint256(_value), 
                new org.web3j.abi.datatypes.generated.Uint8(_symbol), 
                new org.web3j.abi.datatypes.Utf8String(ipfsKey)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> transferOwnership(String newOwner) {
        final Function function = new Function(
                FUNC_TRANSFEROWNERSHIP, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(newOwner)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public List<AuditRequestProcessedEventResponse> getAuditRequestProcessedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(AUDITREQUESTPROCESSED_EVENT, transactionReceipt);
        ArrayList<AuditRequestProcessedEventResponse> responses = new ArrayList<AuditRequestProcessedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            AuditRequestProcessedEventResponse typedResponse = new AuditRequestProcessedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse._asset = (String) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.accepted = (Boolean) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<AuditRequestProcessedEventResponse> auditRequestProcessedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, AuditRequestProcessedEventResponse>() {
            @Override
            public AuditRequestProcessedEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(AUDITREQUESTPROCESSED_EVENT, log);
                AuditRequestProcessedEventResponse typedResponse = new AuditRequestProcessedEventResponse();
                typedResponse.log = log;
                typedResponse._asset = (String) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.accepted = (Boolean) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<AuditRequestProcessedEventResponse> auditRequestProcessedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(AUDITREQUESTPROCESSED_EVENT));
        return auditRequestProcessedEventObservable(filter);
    }

    public List<OwnershipTransferredEventResponse> getOwnershipTransferredEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, transactionReceipt);
        ArrayList<OwnershipTransferredEventResponse> responses = new ArrayList<OwnershipTransferredEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<OwnershipTransferredEventResponse> ownershipTransferredEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, OwnershipTransferredEventResponse>() {
            @Override
            public OwnershipTransferredEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, log);
                OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
                typedResponse.log = log;
                typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<OwnershipTransferredEventResponse> ownershipTransferredEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(OWNERSHIPTRANSFERRED_EVENT));
        return ownershipTransferredEventObservable(filter);
    }

    public static RemoteCall<Auditor> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(Auditor.class, web3j, credentials, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<Auditor> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Auditor.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    public static RemoteCall<Auditor> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(Auditor.class, web3j, transactionManager, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<Auditor> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Auditor.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    @Deprecated
    public static Auditor load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new Auditor(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static Auditor load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new Auditor(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static Auditor load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new Auditor(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static Auditor load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new Auditor(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static class AuditRequestProcessedEventResponse {
        public Log log;

        public String _asset;

        public Boolean accepted;
    }

    public static class OwnershipTransferredEventResponse {
        public Log log;

        public String previousOwner;

        public String newOwner;
    }
}
