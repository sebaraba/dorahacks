package org.web3j.sample.contracts.generated;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;
import rx.Observable;
import rx.functions.Func1;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 3.6.0.
 */
public class Insurer extends Contract {
    private static final String BINARY = "608060405234801561001057600080fd5b5061017e806100206000396000f3006080604052600436106100405763ffffffff7c010000000000000000000000000000000000000000000000000000000060003504166325edf7358114610045575b600080fd5b34801561005157600080fd5b5061007373ffffffffffffffffffffffffffffffffffffffff60043516610075565b005b604080517f4b21076a00000000000000000000000000000000000000000000000000000000815273ffffffffffffffffffffffffffffffffffffffff83166004820152905133918291634b21076a9160248082019260009290919082900301818387803b1580156100e557600080fd5b505af11580156100f9573d6000803e3d6000fd5b50506040805173ffffffffffffffffffffffffffffffffffffffff861681526001602082015281517f0f305b1211f7343bbd57ede4e304191aa5edb5d234c9e85381e75f69bacbbd6e9450908190039091019150a150505600a165627a7a72305820b305232306872fe380dd7708373af6a145759cc4a6f7ec366b649efc49182fea0029";

    public static final String FUNC_PROCESSINSURANCEREQUEST = "processInsuranceRequest";

    public static final Event INSURANCEREQUESTPROCESSED_EVENT = new Event("InsuranceRequestProcessed", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Bool>() {}));
    ;

    @Deprecated
    protected Insurer(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected Insurer(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected Insurer(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected Insurer(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public RemoteCall<TransactionReceipt> processInsuranceRequest(String asset) {
        final Function function = new Function(
                FUNC_PROCESSINSURANCEREQUEST, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(asset)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public List<InsuranceRequestProcessedEventResponse> getInsuranceRequestProcessedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(INSURANCEREQUESTPROCESSED_EVENT, transactionReceipt);
        ArrayList<InsuranceRequestProcessedEventResponse> responses = new ArrayList<InsuranceRequestProcessedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            InsuranceRequestProcessedEventResponse typedResponse = new InsuranceRequestProcessedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse._asset = (String) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.accepted = (Boolean) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<InsuranceRequestProcessedEventResponse> insuranceRequestProcessedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, InsuranceRequestProcessedEventResponse>() {
            @Override
            public InsuranceRequestProcessedEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(INSURANCEREQUESTPROCESSED_EVENT, log);
                InsuranceRequestProcessedEventResponse typedResponse = new InsuranceRequestProcessedEventResponse();
                typedResponse.log = log;
                typedResponse._asset = (String) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.accepted = (Boolean) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<InsuranceRequestProcessedEventResponse> insuranceRequestProcessedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(INSURANCEREQUESTPROCESSED_EVENT));
        return insuranceRequestProcessedEventObservable(filter);
    }

    public static RemoteCall<Insurer> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(Insurer.class, web3j, credentials, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<Insurer> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Insurer.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    public static RemoteCall<Insurer> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(Insurer.class, web3j, transactionManager, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<Insurer> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Insurer.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    @Deprecated
    public static Insurer load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new Insurer(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static Insurer load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new Insurer(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static Insurer load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new Insurer(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static Insurer load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new Insurer(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static class InsuranceRequestProcessedEventResponse {
        public Log log;

        public String _asset;

        public Boolean accepted;
    }
}
