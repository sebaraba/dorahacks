package com.dorahacks.registry.api

import com.dorahacks.registry.modal.AssetModal
import com.dorahacks.registry.modal.InsuranceModal
import com.dorahacks.registry.service.CompanyService
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

/**
 * Controller for explorer block requests.
 */
@Validated
@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/company")
class CompanyController(private val service: CompanyService) {

    @RequestMapping(method = [RequestMethod.POST], path=["/{companyId}/registerAsset"])
    fun registerAsset(@RequestBody assetModal: AssetModal,
                      @PathVariable(value = "companyId")
                      companyId: String) {
        try{
            service.registerAsset(assetModal, companyId)
        } catch (e: Exception) {
            throw ApiException(exception = e, status = HttpStatus.UNPROCESSABLE_ENTITY)
        }
    }

    @RequestMapping(method = [RequestMethod.POST], path=["/{companyId}/askInsurance"])
    fun insureAsset(@RequestBody insuranceModal: InsuranceModal,
                    @PathVariable(value = "companyId")
                    companyId: String): Boolean {
        try{
            service.insureAsset(insuranceModal, companyId)
            return true
        } catch (e: Exception) {
            throw ApiException(exception = e, status = HttpStatus.UNPROCESSABLE_ENTITY)
        }
    }

//    @RequestMapping(method = [RequestMethod.POST], path=["/reportBug"])
//    fun reportBug(@RequestBody body: BugReportModal): String{
//        try{
//            TODO("Ask Insurer for insurance on a piece of equip")
//        } catch (e: Exception) {
//            throw ApiException(exception = e, status = HttpStatus.UNPROCESSABLE_ENTITY)
//        }
//    }

//    @RequestMapping(method = [RequestMethod.GET], path=["/assignAsset"])
//    fun assignAsset(@RequestParam(required = true) assetAddress: String,
//                    @RequestParam(required = true) employeeAddress: String): String{
//        try{
//            TODO("Ask Auditor service for registration acceptance")
//        } catch (e: Exception) {
//            throw ApiException(exception = e, status = HttpStatus.UNPROCESSABLE_ENTITY)
//        }
//    }

    @RequestMapping(method = [RequestMethod.GET], path=["/{companyId}/assets"])
    fun getAssets(@PathVariable(value = "companyId")
                             companyId: String): List<String> {
        try{
            return service.findAssets(companyId)
        } catch (e: Exception) {
            throw ApiException(exception = e, status = HttpStatus.BAD_REQUEST)
        }
    }

    @RequestMapping(method = [RequestMethod.GET], path=["/{companyId}/employees"])
    fun getEmployees(@PathVariable(value = "companyId")
                     companyId: String): List<String> {
        try{
            return service.findEmployees(companyId)
        } catch (e: Exception) {
            throw ApiException(exception = e, status = HttpStatus.BAD_REQUEST)
        }
    }

}
