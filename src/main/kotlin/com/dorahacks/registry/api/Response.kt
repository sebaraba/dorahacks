package com.dorahacks.registry.api

/**
 * REST service response wrapper.
 */
data class Response<T>(
        val id: String? = null,
        val type: String? = null,
        val result: T? = null,
        val error: Error? = null) {

    fun hasError() = error != null

    data class Error(val message: String?)
}
