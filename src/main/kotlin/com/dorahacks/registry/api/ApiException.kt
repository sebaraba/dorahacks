package com.dorahacks.registry.api

import org.springframework.http.HttpStatus

/**
 * Generic exception for HTTP error responses.
 */
class ApiException(
        override val message: String? = null,
        val exception: Throwable? = null,
        val status: HttpStatus) :
        RuntimeException(message, exception)
