//package com.dorahacks.registry.api
//
//import com.dorahacks.registry.service.Service
//import org.hibernate.validator.constraints.URL
//import org.springframework.http.HttpStatus.*
//import org.springframework.http.ResponseEntity
//import org.springframework.validation.annotation.Validated
//import org.springframework.web.bind.annotation.*
//
///**
// * Controller for explorer requests.
// */
//@Validated
//@RestController
//@CrossOrigin(maxAge = 3600)
//@RequestMapping(path = ["/preferences"])
//class UserPreferencesController(private val service: Service) {
//
//    @RequestMapping(method = [RequestMethod.PUT], path = ["/node"])
//    fun updateNode(@RequestBody body: Map<String, @URL String>): ResponseEntity<Unit> {
//        return body["address"]?.run {
//            try {
//                service.updateDefaultNode(this)
//                ResponseEntity.status(NO_CONTENT).build<Unit>()
//            } catch (e: Exception) {
//                throw ApiException(exception = e, status = UNPROCESSABLE_ENTITY)
//            }
//        } ?: throw ApiException(message = "Property \"address\" not found", status = BAD_REQUEST)
//    }
//
//}
