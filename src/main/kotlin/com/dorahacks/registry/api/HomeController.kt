package com.dorahacks.registry.api

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

/**
 * Controller for home page.
 */
@RestController
@RequestMapping("/")
class HomeController {

    @RequestMapping(method = [(RequestMethod.GET)], value = ["/"])
    fun homePage() : ResponseEntity<String>  = ResponseEntity
            .status(HttpStatus.TEMPORARY_REDIRECT)
            .header(HttpHeaders.LOCATION, "swagger-ui.html")
            .build()
}
