package com.dorahacks.registry.api

import com.dorahacks.registry.service.AuditorService
import com.dorahacks.registry.service.Service
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

/**
 * Controller for explorer transaction requests.
 */
@Validated
@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/api")
class ApiController(private val service: Service) {

    @RequestMapping(method = [RequestMethod.GET], path = ["/auditors"])
    fun findAuditors(): List<String> {
        try {
            return service.findAuditors()
        } catch (e: Exception) {
            throw ApiException(exception = e, status = HttpStatus.UNPROCESSABLE_ENTITY)
        }
    }

    @RequestMapping(method = [RequestMethod.GET], path = ["/companies"])
    fun findCompanies(): List<String> {
        try {
            return service.findCompanies()
        } catch (e: Exception) {
            throw ApiException(exception = e, status = HttpStatus.UNPROCESSABLE_ENTITY)
        }
    }

    @RequestMapping(method = [RequestMethod.GET], path = ["/insurers"])
    fun findInsurers(): List<String> {
        try {
            return service.findInsurers()
        } catch (e: Exception) {
            throw ApiException(exception = e, status = HttpStatus.UNPROCESSABLE_ENTITY)
        }
    }

}
