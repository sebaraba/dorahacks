package com.dorahacks.registry.api

import com.dorahacks.registry.service.InsurerService
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

/**
 * Controller for explorer contract requests.
 */
@Validated
@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/insurer")
class InsurerController(private val service: InsurerService) {


}
