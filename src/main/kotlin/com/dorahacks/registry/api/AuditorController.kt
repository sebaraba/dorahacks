package com.dorahacks.registry.api

import com.dorahacks.registry.service.AuditorService
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

/**
 * Controller for explorer transaction requests.
 */
@Validated
@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/auditor")
class AuditorController(private val service: AuditorService) {


}
