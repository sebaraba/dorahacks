package com.dorahacks.registry.config

import com.dorahacks.registry.api.ApiException
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.context.request.WebRequest.SCOPE_REQUEST
import org.springframework.web.servlet.HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE
import org.springframework.web.util.WebUtils.*
import javax.validation.ConstraintViolationException

@Configuration
@ControllerAdvice
@ComponentScan(basePackages = ["com.dorahacks.registry"])
class ExceptionConfiguration {

    @Value("\${debug")
    private var debug: String = "false"

    @Bean
    fun methodValidationPostProcessor() = MethodValidationPostProcessor()

    @ExceptionHandler(ConstraintViolationException::class)
    fun handleConstraintViolationExceptions(ex: ConstraintViolationException, request: WebRequest):
            ResponseEntity<Map<String, Any>> {

        val message = ex.constraintViolations.asSequence()
                .map { "${it.propertyPath} ${it.message}" }
                .joinToString(separator = ",\n")

        val errorAttributes = toErrorAttributes(
                message, ex, UNPROCESSABLE_ENTITY, request)

        return ResponseEntity.status(UNPROCESSABLE_ENTITY)
                .contentType(MediaType.APPLICATION_JSON)
                .body(errorAttributes)
    }

    @ExceptionHandler(NoSuchElementException::class)
    fun handleNoSuchElementExceptions(ex: NoSuchElementException, request: WebRequest):
            ResponseEntity<Map<String, Any>> {

        val errorAttributes = toErrorAttributes(
                null, ex, NOT_FOUND, request)

        return ResponseEntity.status(NOT_FOUND)
                .contentType(MediaType.APPLICATION_JSON)
                .body(errorAttributes)
    }

    @ExceptionHandler(ApiException::class)
    fun handleApiExceptions(ex: ApiException, request: WebRequest):
            ResponseEntity<Map<String, Any>> {

        val errorAttributes = toErrorAttributes(
                ex.message, ex.exception, ex.status, request)

        return ResponseEntity.status(ex.status)
                .contentType(MediaType.APPLICATION_JSON)
                .body(errorAttributes)
    }

    private fun toErrorAttributes(
            message: String?,
            exception: Throwable?,
            status: HttpStatus,
            request: WebRequest):
            MutableMap<String, Any>? {

        message?.also {
            request.setAttribute(ERROR_MESSAGE_ATTRIBUTE, it, SCOPE_REQUEST)
        }

        exception?.also {
            request.setAttribute(ERROR_EXCEPTION_ATTRIBUTE, it, SCOPE_REQUEST)
        }

        request.getAttribute(PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE, SCOPE_REQUEST)?.also {
            request.setAttribute(ERROR_REQUEST_URI_ATTRIBUTE, it, SCOPE_REQUEST)
        }

        request.setAttribute(ERROR_STATUS_CODE_ATTRIBUTE, status.value(), SCOPE_REQUEST)

        return defaultErrorAttributes.getErrorAttributes(request, debug.toBoolean())
    }

    companion object {
        private val defaultErrorAttributes = DefaultErrorAttributes()
    }

}
