package com.dorahacks.registry.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties
class State {

    companion object {
            val companyMap = hashMapOf<String, String>()
            val assetMap = hashMapOf<String, String>()
            val insurerMap = hashMapOf<String, String>()
            val auditorMap = hashMapOf<String, String>()

            val employeeMap = hashMapOf<String, String>()

            val companyIdList = arrayListOf<String>()
            val assetList = arrayListOf<String>()
            val insurerList = arrayListOf<String>()
            val auditorList = arrayListOf<String>()
    }

}
