package com.dorahacks.registry.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import org.web3j.protocol.Web3jService
import org.web3j.protocol.http.HttpService
import org.web3j.protocol.ipc.UnixIpcService
import org.web3j.protocol.ipc.WindowsIpcService
import org.web3j.quorum.Quorum

/**
 * Configuration for blockchian node properties.
 */
@Component
@ConfigurationProperties
class NodeConfiguration {

    lateinit var quorum1: String
    lateinit var quorum2: String
    lateinit var quorum3: String
    lateinit var quorum4: String

    @Value("\${os.name}")
    lateinit var osName: String

    @Bean
    fun quorum1(): Quorum {
        val web3jService: Web3jService = when {
            isHttp() -> HttpService(quorum1)
            isDefaultNode() -> HttpService()
            isWindowsIpc() -> WindowsIpcService(quorum1)
            else -> UnixIpcService(quorum1)
        }
        return Quorum.build(web3jService)
    }

    @Bean
    fun quorum2(): Quorum {
        val web3jService: Web3jService = when {
            isHttp() -> HttpService(quorum2)
            isDefaultNode() -> HttpService()
            isWindowsIpc() -> WindowsIpcService(quorum2)
            else -> UnixIpcService(quorum2)
        }
        return Quorum.build(web3jService)
    }

    @Bean
    fun quorum3(): Quorum {
        val web3jService: Web3jService = when {
            isHttp() -> HttpService(quorum3)
            isDefaultNode() -> HttpService()
            isWindowsIpc() -> WindowsIpcService(quorum3)
            else -> UnixIpcService(quorum3)
        }
        return Quorum.build(web3jService)
    }

    @Bean
    fun quorum4(): Quorum {
        val web3jService: Web3jService = when {
            isHttp() -> HttpService(quorum4)
            isDefaultNode() -> HttpService()
            isWindowsIpc() -> WindowsIpcService(quorum4)
            else -> UnixIpcService(quorum4)
        }
        return Quorum.build(web3jService)
    }

    private fun isDefaultNode() = quorum1.isBlank()

    private fun isHttp() = quorum1.toLowerCase().startsWith("http")

    private fun isWindowsIpc() = osName.toLowerCase().startsWith("win")

}
