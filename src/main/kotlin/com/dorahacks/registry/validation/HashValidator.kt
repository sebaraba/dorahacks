package com.dorahacks.registry.validation

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

internal class HashValidator : ConstraintValidator<Hash, CharSequence> {

    private lateinit var hash: Hash

    override fun initialize(constraintAnnotation: Hash) {
        hash = constraintAnnotation
    }

    override fun isValid(value: CharSequence?, context: ConstraintValidatorContext): Boolean {

        if (context is HibernateConstraintValidatorContext) {
            context.unwrap(HibernateConstraintValidatorContext::class.java)
                    .addMessageParameter("length", hash.value.length)
        }

        return value?.run { hash.value.matches(value) } ?: true
    }

}
