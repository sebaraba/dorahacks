package com.dorahacks.registry.validation

import com.dorahacks.registry.util.HashType
import javax.validation.Constraint
import javax.validation.ReportAsSingleViolation
import kotlin.annotation.AnnotationTarget.*
import kotlin.reflect.KClass

@MustBeDocumented
@Constraint(validatedBy = [HashValidator::class])
@Target(FUNCTION, FIELD, ANNOTATION_CLASS, PROPERTY_GETTER, VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
@ReportAsSingleViolation
annotation class Hash(
        val value: HashType,
        val message: String = "{io.blk.quorum.explorer.validation.Hash}",
        val groups: Array<KClass<out Any>> = [],
        val payload: Array<KClass<out Any>> = [])
