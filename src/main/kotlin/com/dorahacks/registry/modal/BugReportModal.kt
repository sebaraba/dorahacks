package com.dorahacks.registry.modal

/**
 * Account/Contract View Modal
 */
class BugReportModal(var auditorId: String,
                 var balance: String,
                 var transactions: Long,
                 var creationTransaction: String?,
                 var contractId: String?)
