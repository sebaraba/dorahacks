package com.dorahacks.registry.modal

class InsuranceModal(val assetId: String,
                     val insurerId: String)
