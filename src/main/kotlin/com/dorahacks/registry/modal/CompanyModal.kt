package com.dorahacks.registry.modal


/**
 * Block View Modal
 */
data class CompanyModal(var insurerAddress: String,
                        var auditorAddress: String) {

}
