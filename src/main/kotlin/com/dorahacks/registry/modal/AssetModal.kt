package com.dorahacks.registry.modal

import java.math.BigInteger


/**
 * Account/Contract View Modal
 */
class AssetModal(var auditorId: String, val serialNo: BigInteger,
                 val value: BigInteger, val symbol: BigInteger,
                 val ipfsKey: String)
