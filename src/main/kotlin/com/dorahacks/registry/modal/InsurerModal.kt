package com.dorahacks.registry.modal

/**
 * Latest Blocks View Modal
 */
data class InsurerModal(val atBlock: Long, val data: List<BlockRowModal>) {

    data class BlockRowModal(var timestamp: Long?,
                             var number: Long?,
                             var hash: String?,
                             var transactions: Int)
}
