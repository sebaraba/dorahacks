package com.dorahacks.registry.service
import com.dorahacks.registry.config.State
import org.springframework.stereotype.Service

@Service
class Service {
    fun findAuditors(): List<String> {
        return State.auditorList
    }

    fun findCompanies(): List<String> {
        return State.companyIdList
    }

    fun findInsurers(): List<String> {
        return State.insurerList
    }

}
