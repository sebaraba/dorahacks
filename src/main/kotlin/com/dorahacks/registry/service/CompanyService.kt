package com.dorahacks.registry.service

import com.dorahacks.registry.config.State
import com.dorahacks.registry.modal.AssetModal
import com.dorahacks.registry.modal.InsuranceModal
import org.web3j.quorum.Quorum

import org.springframework.stereotype.Service
import org.web3j.protocol.core.DefaultBlockParameterName
import org.web3j.quorum.tx.ClientTransactionManager
import org.web3j.sample.contracts.generated.Asset
import org.web3j.sample.contracts.generated.Company
import org.web3j.tx.gas.DefaultGasProvider
import org.web3j.tx.gas.StaticGasProvider
import java.math.BigInteger
import java.util.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * Service for business logic.
 */
@Service
class CompanyService(private val quorum1: Quorum) {


    private val gasProvider = StaticGasProvider(BigInteger.ZERO, DefaultGasProvider.GAS_LIMIT)

    fun registerAsset(assetModal: AssetModal, companyId: String) {
        val address = State.companyMap[companyId]
        val company = Company.load(address, quorum1, provideTm(), gasProvider)

        val txnReceipt = company.registerAsset(State.auditorMap[assetModal.auditorId], assetModal.serialNo,
                BigInteger.TEN, BigInteger.ONE,
                "123").send()

        val it = company.getAssetRegisteredEvents(txnReceipt)

        it.forEach {
            val a = Asset.load(it._address, quorum1, provideTm(), gasProvider)
            val assetId = UUID.randomUUID().toString()
            State.assetMap[assetId] = a.contractAddress
            State.assetList.add(assetId)
        }

        println("WTF")
    }

    private fun provideTm(): ClientTransactionManager? {
        return ClientTransactionManager(quorum1,
                quorum1.ethAccounts().send().accounts[0],
                null,
                null)
    }

    fun insureAsset(insuranceModal: InsuranceModal, companyId: String) {
        val address = State.companyMap[companyId]
        val company = Company.load(address, quorum1, provideTm(), gasProvider)
        company.insureAsset(State.assetMap[insuranceModal.assetId],
                State.insurerMap[insuranceModal.insurerId]).send()
    }

    fun findAssets(companyId: String): List<String> {
        return State.assetList

    }

    fun findEmployees(companyId: String): List<String> {
        val toReturn = arrayListOf<String>()
        val address = State.companyMap[companyId]
        val company = Company.load(address, quorum1, provideTm(), gasProvider)
        company.employeeAddedEventObservable(DefaultBlockParameterName.EARLIEST,
                DefaultBlockParameterName.LATEST).subscribe {
            toReturn.add(it._address)
        }
        return toReturn
    }
}
