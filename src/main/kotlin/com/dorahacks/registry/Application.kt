package com.dorahacks.registry

import com.dorahacks.registry.config.State
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.web3j.quorum.Quorum
import org.web3j.quorum.tx.ClientTransactionManager
import org.web3j.sample.contracts.generated.Auditor
import org.web3j.sample.contracts.generated.Company
import org.web3j.sample.contracts.generated.Insurer
import org.web3j.tx.gas.DefaultGasProvider
import org.web3j.tx.gas.StaticGasProvider
import java.math.BigInteger
import java.util.*

/**
 * Main entry point of application.
 */
@SpringBootApplication
class Application(private val quorum1: Quorum): ApplicationRunner {

    private val gasProvider = StaticGasProvider(BigInteger.ZERO, DefaultGasProvider.GAS_LIMIT)

    override fun run(args: ApplicationArguments?) {
        val insurerId = UUID.randomUUID().toString()
        State.insurerList.add(insurerId)
        State.insurerMap[insurerId] = "0x341ed42cdfb13b897a198cd1c87ddf070e25e6a1"


        val auditorId = UUID.randomUUID().toString()
        State.auditorList.add(auditorId)
        State.auditorMap[auditorId] = "0x87ec4e85245d901de66c09c96bd53c8146e0c12d"


        val ctm1 = ClientTransactionManager(quorum1,
                quorum1.ethAccounts().send().accounts[0],
                null,
                null)

        val company = Company.load("0x1932c48b2bf8102ba33b4a6b545c32236e342f34",
                quorum1, ctm1,
                gasProvider)

        val companyId = UUID.randomUUID().toString()
        State.companyIdList.add(companyId)
        State.companyMap[companyId] = company.contractAddress

        repeat(3) {
            val employeeId = UUID.randomUUID().toString()
            State.employeeMap[employeeId] = "0x$it"
            company.addEmployee(BigInteger.ZERO, "0x$it").send()
        }
    }

}

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}


