package com.dorahacks.registry.util

import org.web3j.utils.Numeric
import java.util.regex.Pattern

enum class HashType(val length: Int) {

    ADDRESS(40), BLOCK(64), TRANSACTION(64);

    private val pattern = Pattern.compile("^0x[0-9A-Fa-f]{$length}\$")!!

    fun matches(hash: CharSequence): Boolean {
        val prependHexPrefix = Numeric.prependHexPrefix(hash.toString())
        return pattern.matcher(prependHexPrefix).matches()
    }

}
