package com.dorahacks.registry.util

import java.math.BigInteger
import java.util.*

import org.web3j.crypto.Hash
import org.web3j.rlp.RlpEncoder
import org.web3j.rlp.RlpList
import org.web3j.rlp.RlpString
import org.web3j.rlp.RlpType
import org.web3j.utils.Numeric

/**
 * Contract utilities
 */
class Utils {
    companion object {

        private val PAYLOAD_LENGH = 32

        private val PREFIX = byteArrayOf(0xa1.toByte(), 0x65, 'b'.toByte(), 'z'.toByte(), 'z'.toByte(),
                'r'.toByte(), '0'.toByte(), 0x58, 0x20)

        private val POSTFIX = byteArrayOf(0x00, 0x29)

        fun extractSwarmHash(input: String): ByteArray {
            return extractSwarmHash(Numeric.hexStringToByteArray(input))
        }

        private fun extractSwarmHash(input: ByteArray): ByteArray {
            for (i in 0 until input.size - (PAYLOAD_LENGH + POSTFIX.size)) {
                val match = PREFIX.indices
                        .takeWhile { input[i + it] == PREFIX[it] }
                        .any { it == PREFIX.size - 1 }

                if (match
                        && input[i + PREFIX.size + PAYLOAD_LENGH] == POSTFIX[0]
                        && input[i + PREFIX.size + PAYLOAD_LENGH + 1] == POSTFIX[1]) {
                    return Arrays.copyOfRange(
                            input, i + PREFIX.size, i + PREFIX.size + PAYLOAD_LENGH)
                }
            }
            return byteArrayOf()
        }

        fun generateContractAddress(address: ByteArray, nonce: BigInteger): ByteArray {
            val values = ArrayList<RlpType>()

            values.add(RlpString.create(address))
            values.add(RlpString.create(nonce))
            val rlpList = RlpList(values)

            val encoded = RlpEncoder.encode(rlpList)
            val hashed = Hash.sha3(encoded)
            return Arrays.copyOfRange(hashed, 12, hashed.size)
        }

        fun generateContractAddress(address: String, nonce: BigInteger): String {
            val result = generateContractAddress(Numeric.hexStringToByteArray(address), nonce)
            return Numeric.toHexString(result)
        }
    }
}
