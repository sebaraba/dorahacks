package com.dorahacks.registry.util

/**
 * Object representing a range
 */
class Range(value1: Long, value2: Long) {

    val start = if(value1 < value2) value1 else value2
    val end = if(start == value1) value2 else value1

    fun size(): Long = end - start + 1

    override fun toString(): String {
        return "From: $start To: $end"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Range

        if (start != other.start) return false
        if (end != other.end) return false

        return true
    }

    override fun hashCode(): Int {
        var result = start.hashCode()
        result = 31 * result + end.hashCode()
        return result
    }


}
