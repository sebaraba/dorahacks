# Quorum explorer README

## Perquisites

1. Synced geth node
    - Either geth running with ``` --rpcapi personal,db,eth,net,web3 --rpc ``` flags
    - or use `docker-compose up -d quorum1 quorum2 quorum3 quorum4`
1. Mongodb daemon running `docker-compose up -d mongo`

## Run on local machine

1. clone repo
2. navigate to cloned directory in terminal
3. run ``` ./gradlew run ```

[localhost:8080]: http://localhost:8080

## Running in a container
1. run ``` ./gradlew clean createDockerfile ```
1. run `docker-compose up --build api`


## Api endpoints

1. /api/blocks?value=
2. /api/transactions?value=
3. /api/searchBlockByNumber?value=
4. /api/searchAddressByHash?value=
5. /api/searchTransactionByHash?value=

## Adding new blockchain nodes

To add a new instance you need to create a new job step in the `.gitlab-ci.yml`. 
Update the NODE_ENDPOINT to be what the customer has provided
Note the domain is the name of the job step.
You need to do the same for the UI project.

To actualy deploy code to production you need to manually deploy it from within gitlab. Choose the commit pipeline you wish to deploy and hit play.
Make sure everything is working on review before you deploy!!! Then deploy it to production.

### Add user to the mongo db

To connect to the database you need to have kubectl setup and pointing to the azure review cluster.

```
kubectl run --namespace default mongo-mongodb-client --rm --tty -i --image bitnami/mongodb \  
--command -- mongo admin --host mongo-mongodb -u root \  
-p $(kubectl get secret --namespace default mongo-mongodb -o jsonpath="{.data.mongodb-root-password}" | base64 --decode)
```

```use <tenant_name>``` e.g. ```use alastria```

```db.createUser({user:"explorer", pwd:"password", roles: ["readWrite", "dbAdmin"]})```